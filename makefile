.DEFAULT_GOAL := stow

DEVICE=$(shell hostname)

STOWOPTS=--stow --no-folding --dotfiles
STOWPKGS=$(shell ls -1 src)
STOWXPKGS=$(shell ls -1 src-X)

.PHONY: stow
stow: stow-uncommon i3status-config
	stow --dir=src --target=$$HOME $(STOWOPTS) $(STOWPKGS)

.PHONY: stow-uncommon
stow-uncommon:
	stow --dir=src-uncommon --target=src $(STOWOPTS) $(DEVICE)

.PHONY: stow-X
stow-X:
	stow --dir=src-X --target=$$HOME $(STOWOPTS) $(STOWXPKGS)

.PHONY: install-packages
install-packages: install-$(DEVICE)-packages

.PHONY: install-clt-dsk-t-6010-packages
install-clt-dsk-t-6010-packages:
	@$(MAKE) pacman-install-arch-intel
	@$(MAKE) pacman-install-arch-general
	@$(MAKE) pacman-install-arch-more
	@$(MAKE) pacman-install-arch-dev
	@$(MAKE) pacman-install-$(DEVICE)
	@$(MAKE) aur-install-arch-aur

.PHONY: install-syclone-packages
install-syclone-packages:
	@$(MAKE) pacman-install-arch-amd
	@$(MAKE) pacman-install-arch-general
	@$(MAKE) pacman-install-arch-more
	@$(MAKE) pacman-install-arch-dev
	@$(MAKE) pacman-install-arch-amdgpu
	@$(MAKE) pacman-install-$(DEVICE)
	@$(MAKE) aur-install-arch-aur

.PHONY: pacman-install-%
pacman-install-%: packages/%
	doas pacman -S --needed $(shell cat $<)

.PHONY: aur-install-%
aur-install-%: packages/% aur-helper
	paru -S --needed $(shell cat $<)

.PHONY: aur-helper
aur-helper: /usr/bin/paru

/usr/bin/paru:
	git clone --depth 1 https://aur.archlinux.org/paru.git /tmp/paru
	cd /tmp/paru && makepkg -si

.PHONY: i3status-config
i3status-config: src/i3status/.config/i3status/config-generated

src/i3status/.config/i3status/config-generated: src/i3status/.config/i3status/config-device src/i3status/.config/i3status/config-base
	@awk -f ./misc/replace_pattern_with_file.awk\
		-v pattern='^#@config-device@$$'\
		-v replacement_file=$(word 1,$^)\
		$(word 2,$^) > $@

.PHONY: link-firefox-css
link-firefox-css:
	for dir in ~/.mozilla/firefox/*.default*; do \
		mkdir -p $$dir/chrome; \
		ln -sf $$PWD/misc/firefox/*.css $$dir/chrome; \
	done;

.PHONY: symlink-alacritty-to-xterm
symlink-alacritty-to-xterm:
	ln -sf $(shell which alacritty) ~/.local/bin/xterm

.PHONY: install-fish-plugins
install-fish-plugins:
	wget -nv -O ~/.config/fish/functions/replay.fish \
		https://raw.githubusercontent.com/jorgebucaran/replay.fish/main/functions/replay.fish

.PHONY: setup-pass
setup-pass:
	gpg --import ~/sync/misc/gpg/johannesneyer_pub.key
	gpg --allow-secret-key-import --import ~/sync/misc/gpg/johannesneyer_priv.key
	@echo -e "\033[31menter: trust\\\n5\\\n\033[0m"
	gpg --edit-key E33A4A92FE82F873
#	git clone user@hostname:/media/ssd/git/pass.git ~/.password-store
#	git clone ssh://user@domain.tld:port/media/ssd/git/pass.git ~/.password-store
