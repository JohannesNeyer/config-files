#!/usr/bin/expect -f

set timeout 30
set address [lindex $argv 0]

spawn bluetoothctl

send "power off\n"
expect -nocase -re "Controller.*Powered: no"

send "power on\n"
expect -nocase -re "Controller.*Powered: yes"

send "remove $address\n"
expect -nocase -re "has been removed|not available"

send "scan on\n"
expect -nocase -re "NEW.*Device $address"

send "scan off\n"
expect -nocase "discovery stopped"

send "trust $address\n"
expect -nocase "$address Trusted: yes"

proc pair {} {
	send "pair $address\n"
}

pair
expect {
	-nocase "failed to pair" interact {
		pair
		exp_continue
	}
	-nocase "$address Paired: yes"
}

send "connect $address\n"

send "quit\n"
expect eof
