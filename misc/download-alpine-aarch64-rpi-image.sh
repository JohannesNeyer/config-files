#!/bin/sh
set -euo pipefail

path=/media/mmcblk0p2/images
url=https://dl-cdn.alpinelinux.org/alpine/latest-stable/releases/aarch64

tmpfile=$(mktemp)

wget -qO - $url/latest-releases.yaml | yq eval '.[] | select(.title=="Raspberry Pi")' - > $tmpfile

echo latest-release: $(yq eval '.version' $tmpfile) from $(yq eval '.date' $tmpfile)

file=$(yq eval '.file' $tmpfile)

if [ -f $path/$file ]; then
	echo "newest image already downloaded"
else
	doas wget -P "$path" "$url/$file"
	echo "$(yq eval '.sha512' $tmpfile)  $path/$file" | sha512sum -c -
fi

rm $tmpfile
