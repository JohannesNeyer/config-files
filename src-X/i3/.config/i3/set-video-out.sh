#!/bin/bash

if [ "$1" == "" ] || [ "$2" == "" ] || [ "$3" == "" ]; then
	echo "error: missing display parameters"
	exit -1
fi

launchBar() {
	~/.config/polybar/launch_polybar.sh $1
}

monitor=$1
tv=$2
choice=$3

case "$3" in
"1")
	xrandr --output $monitor --mode 2560x1440 --rate 144 --primary
	#xrandr --output $tv --mode 2560x1440 --same-as $monitor
	xrandr --output $tv --off
	echo "Xft.dpi: 96" | xrdb -merge
	launchBar bottom1
	;;
"2")
	xrandr --output $monitor --off --output $tv --auto
	echo "Xft.dpi: 192" | xrdb -merge
	launchBar bottom2
	;;
*)
	echo "error: invalid display choice"
	exit -1
	;;
esac

feh --bg-fill ~/sync/misc/wallpaper/desktopSyclone.png
exit 0
