set -gx LANG en_US.UTF-8
set -gx LC_TIME en_GB.UTF-8
set -gx LC_PAPER de_CH.UTF-8
set -gx LC_MEASUREMENT de_CH.UTF-8

if ! set -q XDG_RUNTIME_DIR
  set -x XDG_RUNTIME_DIR /tmp/(id -u)-runtime-dir
  if ! test -d $XDG_RUNTIME_DIR
    mkdir "$XDG_RUNTIME_DIR"
    chmod 0700 "$XDG_RUNTIME_DIR"
  end
end

set -gx CHROMIUM_USER_FLAGS (cat ~/.config/chromium-flags.conf)

fish_add_path /usr/local/sbin /usr/local/bin /usr/sbin /sbin

source ~/.config/fish/solarized-dark-colors.fish
