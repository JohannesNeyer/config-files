(use-package mode-local
  :commands (setq-mode-local))

(use-package emacs
  :custom
  (custom-file "/tmp/custom.el")
  (inhibit-startup-screen t)
  (server-client-instructions nil)
  (initial-major-mode 'text-mode)
  (initial-scratch-message nil)
  (use-short-answers t)
  (require-final-newline t)
  (visible-bell t)
  (scroll-margin 4)
  (mouse-wheel-progressive-speed nil)
  (show-paren-delay 0)
  (vc-follow-symlinks t)
  (sentence-end-double-space nil)
  (graphviz-dot-auto-indent-on-semi nil)
  (recentf-max-saved-items 100)
  (ediff-split-window-function 'split-window-horizontally)
  (ediff-window-setup-function 'ediff-setup-windows-plain)
  (fill-column 80)
  (c-default-style '((java-mode . "java") (awk-mode . "awk") (other . "linux")))
  (c-doc-comment-style '((java-mode . javadoc) (c-mode . doxygen) (c++-mode . doxygen)))
  (tab-width 4)
  (indicate-empty-lines t)
  (word-wrap t)
  (gdb-many-windows t)
  (gdb-show-main t)
  (whitespace-style '(face tab-mark))
  (backup-by-copying t)
  (enable-recursive-minibuffers t)
  (python-shell-interpreter "ipython")
  (python-shell-interpreter-args "--simple-prompt --quick --HistoryManager.enabled=False --TerminalInteractiveShell.prompts_class=IPython.terminal.prompts.ClassicPrompts")
  (comint-scroll-to-bottom-on-input t)
  (comint-scroll-to-bottom-on-output t)
  ;; (gud-pdb-command-name "ipdb") ;; TODO: try with emacs28?
  :config
  (add-to-list 'comint-output-filter-functions 'comint-truncate-buffer)
  (defvaralias 'c-basic-offset 'tab-width)
  (defvaralias 'cperl-indent-level 'tab-width)
  (line-number-mode)
  (column-number-mode)
  (context-menu-mode)
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (global-display-fill-column-indicator-mode)
  (global-auto-revert-mode)
  (global-whitespace-mode)
  (indent-tabs-mode)
  (save-place-mode)
  (recentf-mode)
  (defconst emacs-auto-save-dir (expand-file-name "~/.emacs.d/auto-save/"))
  (if (not (file-exists-p emacs-auto-save-dir))
      (make-directory emacs-auto-save-dir t))
  (setq backup-directory-alist
        `((".*" . ,emacs-auto-save-dir)))
  (setq-mode-local prog-mode
                   comment-auto-fill-only-comments t
                   show-trailing-whitespace t)
  (setq-mode-local text-mode show-trailing-whitespace t)
  (setq-mode-local conf-mode
                   indent-tabs-mode nil
                   show-trailing-whitespace t)
  (setq-mode-local emacs-lisp-mode indent-tabs-mode nil)
  (setq-mode-local c-mode tab-width 8)
  (setq-mode-local conf-mode tab-width 2)
  (setq-mode-local python-mode fill-column 88)
  :bind
  (("<mouse-8>" . 'previous-buffer)
   ("<mouse-9>" . 'next-buffer)
   ("<f5>" . 'recompile)
   ("M-<tab>" . 'completion-at-point))
  :hook
  (prog-mode . hs-minor-mode)
  (text-mode . flyspell-mode)
  ((prog-mode text-mode) . auto-fill-mode)
  :mode
  ("_defconfig\\'" . conf-mode))

(use-package flymake
  :bind
  (:map flymake-mode-map
        ("M-n" . 'flymake-goto-next-error)
        ("M-p" . 'flymake-goto-prev-error)))

(use-package minions)

(use-package moody
  :custom
  (moody-mode-line-height 20)
  (mode-line-compact t)
  :init
  (let ((line "#284b54")) ; s-line color
    (set-face-attribute 'mode-line nil :overline line :underline line :box nil)
    (set-face-attribute 'mode-line-inactive nil :overline line :underline line :box nil))
  :config
  (moody-replace-vc-mode)
  (moody-replace-mode-line-buffer-identification)
  (minions-mode))

(use-package evil
  :init (setq evil-want-keybinding nil)
  :bind
  (:map evil-normal-state-map
        ("SPC" . hydra-misc/body))
  (:map evil-visual-state-map
        ("SPC" . hydra-misc/body)
        ("s" . 'evil-surround-region))
  :config
  (evil-mode)
  ;(defalias 'forward-evil-word 'forward-evil-symbol)
  :hook
  (artist-mode . evil-emacs-state)
  :custom
  (evil-flash-delay 10)
  (evil-search-wrap-ring-bell t)
  (evil-undo-system 'undo-redo)
  (evil-symbol-word-search t))

(use-package evil-collection
  :after evil
  :custom
  ; breaks tab in org
  ;(evil-collection-outline-bind-tab-p t)
  (evil-collection-key-blacklist '("SPC"))
  :config
  (evil-collection-define-key 'normal 'outline-mode-map
    (kbd "<backtab>") 'outline-cycle-buffer
    (kbd "<tab>") 'outline-toggle-children)
  (evil-collection-define-key 'normal 'magit-blame-mode-map
    (kbd "RET") 'magit-visit-ref)
  (evil-collection-define-key 'normal 'org-mode-map
    (kbd "RET") 'org-open-at-point)
  (evil-collection-init))

(use-package evil-surround
  :config (global-evil-surround-mode))

(use-package evil-matchit
  :config (global-evil-matchit-mode))

(use-package org :straight (:type built-in)
  :custom
  (org-src-window-setup 'current-window)
  (org-src-preserve-indentation t)
  (org-src-tab-acts-natively t)
  (org-startup-folded 'content)
  (org-log-done 'time)
  (org-clock-persist-query-resume nil)
  (org-clock-clocked-in-display nil)
  (org-clock-in-resume t)
  (org-clock-persist t)
  (org-image-actual-width nil)
  ;(org-duration-format 'h:mm)
  (org-duration-format '(("h" . t) (special . 2)))
  (org-pretty-entities t)
  (org-startup-indented t)
  (org-list-allow-alphabetical t)
  :config
  (setq-mode-local org-mode indent-tabs-mode nil)
  (setq-mode-local org-src-mode
                   tab-width 2
                   indent-tabs-mode nil)
  (org-clock-persistence-insinuate)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((python . t)
     (shell . t)
     (C . t)
     (dot . t)
     (lisp . t))))

(use-package solarized-theme
  :custom
  (solarized-scale-org-headlines nil)
  (solarized-use-variable-pitch nil)
  (x-underline-at-descent-line t)
  :config
  (set-face-attribute 'nobreak-space nil :underline nil) ; for markdown in eldoc
  (set-face-attribute 'org-block-begin-line nil :underline nil)
  (set-face-attribute 'org-block-end-line nil :overline nil))

(load "~/.emacs.d/init-device.el")

(use-package outline-minor-faces
  :after outline
  :hook
  (outline-minor-mode . outline-minor-faces-add-font-lock-keywords)
  ; for some reason outline-minor-1 is ugly when emacs is launched in daemon mode
  (outline-minor-mode
   . (lambda () (set-face-attribute 'outline-minor-1 nil :background nil))))

(use-package backline
  :after outline
  :config (advice-add 'outline-flag-region :after 'backline-update))

(use-package which-key
  :config (which-key-mode))

(use-package eldoc :defer
  :custom
  (eldoc-idle-delay 0.1)
  :config
  ; make ( trigger eldoc in c-mode
  (eldoc-add-command 'c-electric-paren))

(use-package eglot :defer
  :custom
  (eglot-extend-to-xref t)
  :config
  (add-to-list 'eglot-server-programs '((c-mode c++-mode) .
                                        ("clangd" "--header-insertion=never" "--clang-tidy")))
  (add-to-list 'eglot-server-programs '(rust-mode . ("rust-analyzer")))
  (add-to-list 'eglot-server-programs '((tex-mode texinfo-mode bibtex-mode) . ("texlab")))
  (add-to-list 'eglot-server-programs '(context-mode) . ("digestif"))
  (setq-default eglot-workspace-configuration
                '((:pylsp (plugins
                           ;; (jedi_completion (include_params . t) (fuzzy . t))
                           ;; (pydocstyle (enabled . t))
                           ;; (pylint (enabled . t))
                           (pycodestyle (maxLineLength . 88))))
                  (:gopls (staticcheck . t))))
  :hook
  (c-mode . eglot-ensure)
  (c++-mode . eglot-ensure)
  (rust-mode . eglot-ensure)
  (python-mode . eglot-ensure)
  (js-mode . eglot-ensure)
  (typescript-mode . eglot-ensure)
  (TeX-mode . eglot-ensure)
  :bind (:map eglot-mode-map
              ("C-c r" . 'eglot-rename)
              ("C-c o" . 'eglot-code-action-organize-imports)
              ("C-c h" . 'eldoc)))

(use-package editorconfig
  :config
  (editorconfig-mode))

(use-package magit :defer
  :custom
  (magit-diff-refine-hunk 'all)
  (magit-blame-echo-style 'headings)
  :bind
  (:map magit-revision-mode-map ("SPC" . nil))
  (:map magit-mode-map ("SPC" . nil)))

(use-package git-modes :defer
  :mode ("\\.dockerignore\\'" . gitignore-mode))

(use-package hl-todo
  :hook (prog-mode . hl-todo-mode))

(use-package magit-todos :defer
  :hook (magit-mode . magit-todos-mode))

(use-package smart-tabs-mode
  :config
  (smart-tabs-insinuate 'c 'c++ 'java 'javascript 'cperl 'ruby 'nxml))

(use-package avy :defer
  :bind (("C-:" . 'avy-goto-char)))

(use-package orderless
  :custom
  ;; (orderless-matching-styles '(orderless-flex))
  (orderless-component-separator "[ &]")
  (orderless-smart-case t)
  (completion-ignore-case t)
  (completion-styles '(orderless))
  (completion-category-defaults nil)
  (completion-category-overrides
   '((eglot (styles . (orderless)))
     (file (styles . (partial-completion))))))

(use-package marginalia
  :bind (("M-A" . marginalia-cycle))
  :init (marginalia-mode)) ; TODO: why init?

(use-package embark
  :bind
  (("C-." . embark-act)
   ("M-." . embark-dwim)))

(use-package savehist
  :config (savehist-mode))

(straight-use-package '( vertico :files (:defaults "extensions/*")
                                 :includes (vertico-buffer
                                            vertico-directory
                                            vertico-flat
                                            vertico-indexed
                                            vertico-mouse
                                            vertico-quick
                                            vertico-repeat
                                            vertico-reverse)))

(use-package vertico
  :config
  (vertico-mode)
  :custom
  ;; Grow and shrink the Vertico minibuffer
  (vertico-resize t)
  (vertico-cycle t))

(use-package vertico-directory
  :straight nil
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package vertico-quick
  :straight nil
  :bind (:map vertico-map
              ("M-q" . vertico-quick-insert)
              ("C-q" . vertico-quick-exit)))

(use-package consult
  :bind (;; C-c bindings (mode-specific-map)
         ("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c k" . consult-kmacro)
         ;; C-x bindings (ctl-x-map)
         ("C-x M-:" . consult-complex-command)
         ("C-x b" . consult-buffer)
         ("C-x 4 b" . consult-buffer-other-window)
         ("C-x 5 b" . consult-buffer-other-frame)
         ("C-x r b" . consult-bookmark)
         ("C-x p b" . consult-project-buffer)
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)
         ("<help> a" . consult-apropos)
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)
         ("M-g g" . consult-goto-line)
         ("M-g M-g" . consult-goto-line)
         ("M-g o" . consult-outline)
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings (search-map)
         ("M-s d" . consult-find)
         ("M-s D" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s m" . consult-multi-occur)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)
         ("M-s e" . consult-isearch-history)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)
         ("M-r" . consult-history))
  :init
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)
  (advice-add #'register-preview :override #'consult-register-window)
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)
  :config
  (consult-customize
   consult-theme
   :preview-key '(:debounce 0.2 any)
   consult-buffer consult-ripgrep consult-git-grep consult-grep consult-bookmark
   consult-recent-file consult-xref consult--source-bookmark
   consult--source-recent-file consult--source-project-recent-file
   :preview-key (kbd "M-."))
  (setq consult-narrow-key "<"))

(use-package corfu
  :bind
  (:map corfu-map
        ("TAB" . corfu-next)
        ([tab] . corfu-next)
        ("S-TAB" . corfu-previous)
        ([backtab] . corfu-previous))
  :custom
  (corfu-cycle t)
  :init
  (global-corfu-mode))

(use-package cape
  :bind (("C-c p p" . completion-at-point)
         ("C-c p t" . complete-tag)
         ("C-c p d" . cape-dabbrev)
         ("C-c p f" . cape-file)
         ("C-c p k" . cape-keyword)
         ("C-c p s" . cape-symbol)
         ("C-c p a" . cape-abbrev)
         ("C-c p i" . cape-ispell)
         ("C-c p l" . cape-line)
         ("C-c p w" . cape-dict)
         ("C-c p \\" . cape-tex)
         ("C-c p &" . cape-sgml)
         ("C-c p r" . cape-rfc1345))
  :init
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-keyword))

(use-package dabbrev
  :bind (("M-/" . dabbrev-completion)
         ("C-M-/" . dabbrev-expand)))

(use-package kind-icon
  :after corfu
  :custom (kind-icon-default-face 'corfu-default)
  :config (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package yasnippet-snippets)

(use-package yasnippet
  :after yasnippet-snippets
  :config (yas-global-mode))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package markdown-mode
  :mode ("\\.md\\'" . gfm-mode)
  :hook (markdown-mode . flyspell-mode)
  :custom
  (markdown-fontify-code-blocks-natively t)
  (markdown-command "cmark-gfm --unsafe -e footnotes -e table -e strikethrough -e autolink -e tagfilter -e tasklist")
  :config
  (set-face-attribute 'markdown-line-break-face nil :underline nil))

(use-package tex :defer :straight auctex
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t)
  (TeX-master nil)
  (TeX-source-correlate-mode t)
  (TeX-engine 'xetex)
  (ConTeXt-Mark-version "IV")
  :config
  (add-to-list 'TeX-view-program-selection '(output-pdf "Zathura")))

(use-package nroff-mode :defer
  :mode "\\.mom\\'"
  :bind (:map nroff-mode-map ("C-c C-c" . 'recompile))
  :hook
  ; TODO: only do this for .mom files
  (nroff-mode . (lambda()
                  (setq outline-regexp "\\.HEADING[ ]+[1-7]+ "
                        comment-start "\\# "
                        outline-level
                        (lambda()
                          (save-excursion
                            (looking-at outline-regexp)
                            (skip-chars-forward ".HEADING ")
                            (string-to-number
                             (buffer-substring (point) (+ 1 (point)))))))))
  (nroff-mode . outline-minor-mode))

(use-package cmake-mode :straight (:type built-in)
  :config
  (require 'cmake-mode)
  (setq-mode-local cmake-mode indent-tabs-mode nil))

(use-package cmake-font-lock
  :hook (cmake-mode . cmake-font-lock-activate))

(use-package docker-compose-mode :defer)

(use-package dockerfile-mode :defer)

(use-package dts-mode :defer :straight (:repo "emacs-straight/dts-mode")
  :mode "\\.overlay\\'")

(use-package yaml-mode :defer)

(use-package kconfig-mode
  :mode "\\.\\(board\\|defconfig\\)\\'"
  :config
  (require 'kconfig-mode)
  (setq-mode-local kconfig-mode indent-line-function 'insert-tab))

(use-package scad-mode :defer
  :config (setq-mode-local scad-mode indent-tabs-mode nil))

(use-package rust-mode :defer
  :bind (:map rust-mode-map ("C-c C-c" . rust-run))
  :config
  (setq-mode-local rust-mode indent-tabs-mode nil))

(use-package ob-rust :defer)

(use-package rfc-mode :defer)

(use-package plantum-mode :defer
  :straight (:type git :host github :repo "skuro/plantuml-mode")
  :custom
  (plantuml-indent-level 4)
  (plantuml-jar-path "/usr/bin/plantuml")
  (plantuml-default-exec-mode 'executable))

(use-package topsy
  :hook (prog-mode . topsy-mode))

(use-package titlecase :defer)

(use-package jupyter :defer)

(use-package pkgbuild-mode :defer
  :custom
  (pkgbuild-update-sums-on-save nil))

(use-package haskell-mode :defer
  :bind (:map haskell-mode-map ("C-c C-c" . haskell-process-load-or-reload)))

(use-package mw-thesaurus :defer)

(use-package hydra
  :bind ("C-c c" . hydra-clock/body))

(defhydra hydra-clock (:color blue)
  ("c" org-clock-cancel "cancel" :color pink)
  ("d" org-clock-display "display")
  ("e" org-clock-modify-effort-estimate "effort")
  ("i" org-clock-in "in")
  ("j" org-clock-goto "jump")
  ("o" org-clock-out "out")
  ("r" org-clock-report "report"))

(defhydra hydra-misc (:color blue)
  ("b" evil-buffer "switch buffer")
  ("h" previous-buffer "prev buffer")
  ("l" next-buffer "next buffer")
  ("d" (flymake-show-diagnostics-buffer) "flymake diags")
  ("c" (lambda () (interactive) (save-buffer) (recompile)) "recompile")
  ("e" eval-region "eval region")
  ("ff" consult-find "project find file")
  ("fr" consult-ripgrep "rg")
  ("fg" consult-git-grep "project find string")
  ("fs" consult-line "consult line")
  ("g" magit-status "magit")
  ("q" save-buffers-kill-emacs "save and quit")
  ("r" revert-buffer "revert buffer")
  ("s" yas-insert-snippet "insert snippet")
  ("u" straight-pull-all "update packages"))


; Make org-src-tab-acts-natively work with evil
; https://github.com/emacs-evil/evil/issues/1288

(defun evil-org-insert-state-in-edit-buffer (fun &rest args)
  "Bind `evil-default-state' to `insert' before calling FUN with ARGS."
  (let ((evil-default-state 'insert)
        ;; Force insert state
        evil-emacs-state-modes
        evil-normal-state-modes
        evil-motion-state-modes
        evil-visual-state-modes
        evil-operator-state-modes
        evil-replace-state-modes)
    (apply fun args)
    (evil-refresh-cursor)))

(advice-add 'org-babel-do-key-sequence-in-edit-buffer
            :around #'evil-org-insert-state-in-edit-buffer)
