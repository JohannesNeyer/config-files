if test -e ~/.config/fish/config-device.fish
    source ~/.config/fish/config-device.fish
end

fish_add_path ~/.local/bin

set -x EDITOR nvim
set -x GIT_EDITOR nvim
set -x BROWSER firefox
set -x CMAKE_EXPORT_COMPILE_COMMANDS ON
set -x MPD_HOST ~/.cache/mpd/socket
set -x JQ_COLORS "1;36:0;39:0;39:0;39:0;32:1;39:1;39"

set fish_greeting

set fish_cursor_default block blink
set fish_cursor_insert line blink
set fish_cursor_replace_one underscore blink

set -g __fish_git_prompt_showdirtystate yes
set -g __fish_git_prompt_showstashstate yes
set -g __fish_git_prompt_showcolorhints yes
set -g __fish_git_prompt_showupstream auto
set -g __fish_git_prompt_char_stateseparator ' '
set -g __fish_git_prompt_use_informative_chars yes
set -g __fish_git_prompt_char_dirtystate '✘'
set -g __fish_git_prompt_char_stashstate '$'
set -g __fish_git_prompt_char_upstream_equal ''
set -g __fish_git_prompt_char_upstream_ahead '↑'
set -g __fish_git_prompt_char_upstream_behind '↓'
set -g __fish_git_prompt_char_upstream_diverged '↓↑'

# use fish_key_reader to get bindings
bind \e\[3\;5~ kill-word
bind \b backward-kill-word

set -ax SKIM_DEFAULT_OPTIONS "--reverse"
set SKIM_CTRL_T_COMMAND "command fd -H . \$dir"
set SKIM_ALT_C_COMMAND "command fd -H -td . \$dir"

if test -e /usr/share/skim/key-bindings.fish
   source /usr/share/skim/key-bindings.fish
end
skim_key_bindings
bind \cs skim-file-widget
bind \cr skim-history-widget
bind \ec skim-cd-widget

abbr --add --global sudo doas
abbr --add --global sudoedit doasedit
abbr --add --global o open

function genpatch ; git diff HEAD > (basename -s.git (git remote get-url origin))_(date -I).patch; end
function less  ; command less -Ri $argv; end
function info  ; command info --vi-keys $argv; end
function emc   ; emacsclient --alternate-editor="" --no-wait --create-frame $argv; end
function e
    if test -z "$argv"
       emc
       return
    end
    for arg in $argv
        string match -r '(?<file>[^:]+):(?<line>[0-9]+)$' $arg > /dev/null
        if test -n "$line"
           emc +$line $file
        else
           emc $arg
        end
    end
end
function ex    ; emc --eval "(save-buffers-kill-emacs)"; end
function et    ; emc --eval "(find-file \"$argv[1]\")" \
                     --eval "(treemacs)" \
                     --eval "(select-window (next-window))" > /dev/null; end
function ediff ; emc --eval "(ediff \"$argv[1]\" \"$argv[2]\")"; end
function doasedit ; emc --eval "(find-file \"/doas::$argv\")"; end
function fd    ; command fd --hidden $argv; end
function rm    ; command rm --interactive=once $argv; end
function l     ; command ls --color=auto -A --group-directories-first $argv; end
function ll    ; command ls --color=auto -lAh --group-directories-first $argv; end
function rg    ; command rg --smart-case $argv; end
function rgu   ; rg -uu $argv; end
function mv    ; command mv --interactive $argv; end
function cp    ; command cp --interactive $argv; end
function vi    ; command nvim $argv; end
function ip    ; command ip --color $argv; end
function g     ; git status; end
function gi    ; git status --ignored; end
function gl    ; git log --graph --oneline --branches; end
function gb    ; git branch -a; end
function gr    ; git remote -v; end
function gc    ; git clone $argv; end
function gf    ; git pull --autostash; end
function gp    ; git push; end
function gz    ; git stash; end
function ipyrun ; ipython --pdb -c "%run $argv"; end

function runinx
    env -u WAYLAND_DISPLAY \
        -u XDG_SESSION_TYPE \
        -u XDG_CURRENT_DESKTOP \
        GDK_BACKEND=x11 \
        QT_QPA_PLATFORM=xcb \
        $argv
end

function magit
    set -l lisp "(magit-status \""(git rev-parse --show-toplevel)"\")"
    emc --eval $lisp --eval "(delete-other-windows)" > /dev/null
end

function egdb
     set -l lisp '(gdb "arm-none-eabi-gdb -i=mi '$argv' -ex \"target extended-remote :2331\"
                                                        -ex \"monitor halt\"
                                                        -ex \"monitor reset\"
                                                        -ex load
                                                        -ex \"thbreak main\"
                                                        ")'
     emc --eval  $lisp \
         --eval "(menu-bar-mode 1)" \
         --eval "(tool-bar-mode 1)" \
         > /dev/null
end

if status is-login
    set -x QT_QPA_PLATFORMTHEME qt5ct
    if test -z "$DISPLAY" -a (tty) = /dev/tty1
        set -x XDG_SESSION_TYPE wayland
        set -x XDG_CURRENT_DESKTOP sway
        set -x QT_QPA_PLATFORM wayland
        set -x QT_WAYLAND_DISABLE_WINDOWDECORATION 1
        set -x MOZ_ENABLE_WAYLAND 1
        #set -x _JAVA_AWT_WM_NONREPARENTING 1 #?
        #set -x SDL_VIDEODRIVER wayland
        exec sway
    else if test -z "$DISPLAY" -a (tty) = /dev/tty2
        exec startx -- -keeptty
    end
end

# Local Variables:
# indent-tabs-mode: nil
# End:
