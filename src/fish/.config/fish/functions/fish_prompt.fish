# Modified version of the default prompt
# /usr/share/fish/functions/fish_prompt.fish
function fish_prompt --description 'Write out the prompt'
    set -l last_pipestatus $pipestatus
    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.
    set -l normal (set_color normal)

    # If we're running via SSH, show host.
    # /usr/share/fish/functions/prompt_login.fish
    if set -q SSH_TTY
        # set prefix (prompt_login)' '
        set prefix "$USER" $normal @ (prompt_hostname) $normal ' '
    end

    # Write pipestatus
    # If the status was carried over (e.g. after `set`), don't bold it.
    set -l bold_flag --bold
    set -q __fish_prompt_status_generation; or set -g __fish_prompt_status_generation $status_generation
    if test $__fish_prompt_status_generation = $status_generation
        set bold_flag
    end
    set __fish_prompt_status_generation $status_generation
    set -l status_color (set_color $fish_color_status)
    set -l statusb_color (set_color $bold_flag $fish_color_status)
    set -l prompt_status (__fish_print_pipestatus "[" "]" "|" "$status_color" "$statusb_color" $last_pipestatus)

    echo -n -s $prefix (prompt_pwd) $normal (fish_git_prompt) $normal " "$prompt_status $suffix " "
end

# /usr/share/fish/functions/fish_default_mode_prompt.fish
function fish_mode_prompt
    switch $fish_bind_mode
        case default
            set_color red
        case insert
            set_color green
        case replace_one
            set_color green
        case replace
            set_color cyan
        case visual
            set_color magenta
    end
end
