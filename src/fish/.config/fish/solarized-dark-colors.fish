# solarized dark theme from `fish_config colors`
set fish_color_autosuggestion 586e75
set fish_color_cancel -r
set fish_color_command 93a1a1
set fish_color_comment 586e75
set fish_color_cwd green
set fish_color_cwd_root red
set fish_color_end 268bd2
set fish_color_error dc322f
set fish_color_escape 00a6b2
set fish_color_history_current --bold
set fish_color_host normal
set fish_color_match --background=brblue
set fish_color_normal normal
set fish_color_operator 00a6b2
set fish_color_param 839496
set fish_color_quote 657b83
set fish_color_redirection 6c71c4
set fish_color_search_match bryellow --background=black
set fish_color_selection white --bold --background=brblack
set fish_color_user brgreen
set fish_color_valid_path --underline
set fish_pager_color_completion B3A06D
set fish_pager_color_description B3A06D
set fish_pager_color_prefix cyan --underline
set fish_pager_color_progress brwhite --background=cyan
set -ax SKIM_DEFAULT_OPTIONS --color=dark
set -x BAT_THEME "Solarized (dark)"
set -x LESS_TERMCAP_mb (set_color -o red)          # begin blinking
set -x LESS_TERMCAP_md (set_color -o green)        # begin bold
set -x LESS_TERMCAP_me \e'[0m'                     # end mode
set -x LESS_TERMCAP_so (set_color -b yellow black) # begin standout-mode - info box
set -x LESS_TERMCAP_se \e'[0m'                     # end standout-mode
set -x LESS_TERMCAP_us (set_color -u brcyan)       # begin underline
set -x LESS_TERMCAP_ue \e'[0m'                     # end underline
