#!/bin/bash

set -eEuo pipefail

trap 'echo "{\"state\":\"Critical\", \"text\": \" failed setting sink\"}" && exit' ERR

declare -a devs
declare -a profiles
case "$(hostname)" in
"clt-dsk-t-6010")
	devs[0]="pci-0000_00_1f.3"
	profiles[0]="output:analog-stereo"
	devs[1]="usb-Plantronics_Plantronics_Blackwire_325.1_CE1A3B73A1E56449A56B926A23B0B7D4-00"
	profiles[1]="output:analog-stereo+input:analog-stereo"
	devs[2]="94_DB_56_17_F9_33" # WH-1000XM3
	profiles[2]=""
	;;
"syclone")
	devs[0]="pci-0000_07_00.1"
	profiles[0]=$(pactl list | sed -En "s/(output:hdmi-.+?): Digital Stereo.+?available: yes.*/\1/p")
	devs[1]="pci-0000_09_00.4"
	profiles[1]="output:analog-stereo"
	;;
esac

setSpeakers() {
	i=0
	setFromId
}

setHeadphones() {
	i=1
	setFromId
}

setBluetooth() {
	i=2
	setFromId
}

cycle() {
	defaultSink=$(pactl info | sed -En -e "s/Default Sink: [^.]+\.([^.]+).*/\1/p")

	for i in "${!devs[@]}"; do
		if [[ ${devs[$i]} == *$defaultSink* ]]; then
			break;
		fi
	done

	availableSinks=($(pactl list short sinks | grep -Eo "[[:alpha:]]*_output[^[:space:]]*"))

	for n in "${!devs[@]}"; do
		i=$((($i + 1) % ${#devs[@]}))

		for availableSink in "${availableSinks[@]}"; do
			if [[ $availableSink == *${devs[$i]}* ]]; then
				setFromId
				success
				exit 0
			fi
		done
	done
	exit -1
}

setFromId() {
	dev=${devs[$i]}
	profile=${profiles[$i]}
	if ! [ -z "$profile" ]; then
		setProfile
	fi
	setSink
}

setProfile() {
	pactl set-card-profile alsa_card.$dev $profile
}

setSink() {
	sink=$(pactl list short sinks | grep -Eo "[[:alpha:]]*_output\.$dev\.[^[:space:]]*")
	pactl set-default-sink $sink
}

success() {
	echo "{\"state\":\"Idle\", \"text\": \"⮂\"}"
}

if [ $# -eq 0 ]; then
	success
	exit 0
fi

if [ "$1" == "headphones" ]; then
	setHeadphones
elif [ "$1" == "speakers" ]; then
	setSpeakers
elif [ "$1" == "bluetooth" ]; then
	setBluetooth
elif [ "$1" == "cycle" ]; then
	cycle
else
	exit -1
fi
