#!/bin/bash

set -eEuo pipefail

trap 'echo "{\"state\":\"Critical\", \"text\": \" failed switching port\"}" && exit' ERR

setPort() {
	pactl set-sink-port @DEFAULT_SINK@ $port
}

if [ "$1" == "headphones" ]; then
	port=analog-output-headphones
	text=" Headphones"
elif [ "$1" == "speaker" ]; then
	port=analog-output-speaker
	text=" Speaker"
else
	echo "{\"state\":\"Critical\", \"text\": \" invalid arg: $1\"}"
	exit -1
fi

setPort
echo "{\"text\": \" $text\"}"

exit 0
