import pandas as pd
from pylab import *

try:
    import sympy as sy
    x, y, z, t = sy.symbols('x y z t')
    k, m, n = sy.symbols('k m n', integer=True)
    f, g, h = sy.symbols('f g h', cls=sy.Function)
    sy.init_printing()
except ImportError:
    pass
