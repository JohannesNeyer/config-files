BEGIN {
	p = 0;
	idx = 0;
};

(p == 0 && /^Audio/) {
	p = 1;
	next;
};

(p == 1 && /Sinks/) {
	p = 2;
	next;
};

(p == 2 && /[[:punct:]]*[[:digit:]]+\./) {
	match($0, /[[:digit:]]+\./);
	id = substr($0, RSTART, RLENGTH - 1);
	if ($0 ~ /[[:punct:]]*\*/) {
		defaultidx = idx;
	}
	ids[idx++] = id;
	next;
};

(p == 2) {
	exit;
};

END {
	print ids[(defaultidx + 1) % length(ids)];
};
